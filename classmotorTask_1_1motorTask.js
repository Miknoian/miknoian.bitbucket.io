var classmotorTask_1_1motorTask =
[
    [ "__init__", "classmotorTask_1_1motorTask.html#a5b0fba9f9d03868cefc8bcc628345821", null ],
    [ "disable", "classmotorTask_1_1motorTask.html#aaac84b9d03393539bff3654c1db09ba6", null ],
    [ "enable", "classmotorTask_1_1motorTask.html#a3f8b7c5e9302fe0c8dc0b314145a1e6f", null ],
    [ "get_duty", "classmotorTask_1_1motorTask.html#a7b627b8b35da3748748fdd52eca91b69", null ],
    [ "set_duty", "classmotorTask_1_1motorTask.html#ace159d3f3824fd4e482ae2babb0fe08e", null ],
    [ "chnl1", "classmotorTask_1_1motorTask.html#a2977ed62054cff08f22f6da90a95b996", null ],
    [ "chnl2", "classmotorTask_1_1motorTask.html#a759d749384f49fe8f738c65fb54ddb6c", null ],
    [ "duty", "classmotorTask_1_1motorTask.html#a32c027368c20a508a2794a08ac395c71", null ],
    [ "IN1_pin", "classmotorTask_1_1motorTask.html#a5964a42ccae0c88d8c7b3b97b3687a9f", null ],
    [ "IN2_pin", "classmotorTask_1_1motorTask.html#a6d9bdc4bf4842990dc9ce1222f74a7f2", null ],
    [ "nSLEEP_pin", "classmotorTask_1_1motorTask.html#a886cbed237a43beaf922fafb49f7cd05", null ],
    [ "nSLEEP_pinRun", "classmotorTask_1_1motorTask.html#a2dec33d11007880b70f37d02fc8a6edd", null ],
    [ "tch1", "classmotorTask_1_1motorTask.html#a23739092506d770dc9b28cb7699ed48f", null ],
    [ "tch2", "classmotorTask_1_1motorTask.html#ad211e79e7eac8599d67d31bd3a246272", null ]
];