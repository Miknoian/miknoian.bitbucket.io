var annotated_dup =
[
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "controlTask", null, [
      [ "controlTask", "classcontrolTask_1_1controlTask.html", "classcontrolTask_1_1controlTask" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "dataTask", null, [
      [ "dataTask", "classdataTask_1_1dataTask.html", "classdataTask_1_1dataTask" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "encoderTask", null, [
      [ "encoderTask", "classencoderTask_1_1encoderTask.html", "classencoderTask_1_1encoderTask" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "DRV8847", "classmotorDriver_1_1DRV8847.html", "classmotorDriver_1_1DRV8847" ],
      [ "DRV8847_channel", "classmotorDriver_1_1DRV8847__channel.html", "classmotorDriver_1_1DRV8847__channel" ]
    ] ],
    [ "motorTask", null, [
      [ "motorTask", "classmotorTask_1_1motorTask.html", "classmotorTask_1_1motorTask" ]
    ] ],
    [ "simonTask", null, [
      [ "simonTask", "classsimonTask_1_1simonTask.html", "classsimonTask_1_1simonTask" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchDriver", null, [
      [ "touchDriver", "classtouchDriver_1_1touchDriver.html", "classtouchDriver_1_1touchDriver" ]
    ] ]
];