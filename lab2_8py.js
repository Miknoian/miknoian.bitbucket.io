var lab2_8py =
[
    [ "onButtonPressFCN", "lab2_8py.html#afa06dc06c7a8ec3438bbdc2e318e84a4", null ],
    [ "advance_flag", "lab2_8py.html#aef827abb2556d00a7fc9ed828711712f", null ],
    [ "ButtonInt", "lab2_8py.html#a6668e6a9443dad33aadbce3769de7d48", null ],
    [ "duty", "lab2_8py.html#a61e2c6510346fa3660ea3e2d3b304d94", null ],
    [ "elapsed", "lab2_8py.html#ace23c43d95f7cfa84123a7fa51f33268", null ],
    [ "pattern_2", "lab2_8py.html#ab6596831667ec7505e4f5d863babfd43", null ],
    [ "pinA5", "lab2_8py.html#ae80cdd9ecc09f16480c7f50566f642bb", null ],
    [ "pinC13", "lab2_8py.html#a8f4c58cf5ae4822cd2cf27257938ddb8", null ],
    [ "start", "lab2_8py.html#afa45ab9ed92f1c8d4de17f9332f76b24", null ],
    [ "state", "lab2_8py.html#a556358ddf3036201e8e30319d25717b2", null ],
    [ "t2ch1", "lab2_8py.html#a26a0a28570c33791ad97bc9d7ef26039", null ],
    [ "tim2", "lab2_8py.html#a4d49995e30c0a58714068e6d082aa320", null ],
    [ "time_now", "lab2_8py.html#ad9432a3172984fee96afaf267ea9339b", null ],
    [ "time_s1_start", "lab2_8py.html#a08dfdce1f5d3bc8b70acf2a9c13a4d23", null ],
    [ "time_s2_start", "lab2_8py.html#af410a5c8be2b5beb831602f35ddb9283", null ],
    [ "time_s3_start", "lab2_8py.html#aaca0817077cf8aebd2c9efdc2687e08c", null ]
];