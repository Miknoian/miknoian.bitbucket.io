/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mark Iknoian Mechatronics Lab Portfolio", "index.html", [
    [ "ME 305", "page1.html", [
      [ "Collection of ME 305 content", "page1.html#sec1", [
        [ "Lab 1", "page1.html#subsec3051", null ],
        [ "Lab 2", "page1.html#subsec3052", null ],
        [ "Lab 3", "page1.html#subsec3053", null ],
        [ "Final Lab", "page1.html#subsec3054", null ]
      ] ]
    ] ],
    [ "ME 405", "page2.html", [
      [ "Collection of ME 405 content", "page2.html#sec2", [
        [ "HW2", "page2.html#subsec4052", null ],
        [ "HW4", "page2.html#subsec4053", null ],
        [ "Lab 1", "page2.html#subsec4051", null ],
        [ "Lab 3", "page2.html#subsec4054", null ],
        [ "Lab 4", "page2.html#subsec4055", null ],
        [ "Final Lab", "page2.html#subsec4056", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"405__hw2_8py.html",
"classsimonTask_1_1simonTask.html#acc2117e54412394d8ffd2b571c6f6b9b"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';