var classmotorDriver_1_1DRV8847__channel =
[
    [ "__init__", "classmotorDriver_1_1DRV8847__channel.html#a62b6fcecae419c72527978d2e1f902b9", null ],
    [ "set_duty", "classmotorDriver_1_1DRV8847__channel.html#ae7f4583898d681e3ba3da654db687b4c", null ],
    [ "channel1", "classmotorDriver_1_1DRV8847__channel.html#af0599ace22b3bdba95e6c1b3042ee3a7", null ],
    [ "channel2", "classmotorDriver_1_1DRV8847__channel.html#a94f29bd8aaec2d79f93c976079d6a159", null ],
    [ "duty", "classmotorDriver_1_1DRV8847__channel.html#a03f6bbdb22802d9fe737e793c4a9d7d4", null ],
    [ "INx_pin", "classmotorDriver_1_1DRV8847__channel.html#a8875ffa270d81e7ae5443cb86fb522fd", null ],
    [ "INxy_timer", "classmotorDriver_1_1DRV8847__channel.html#adc95911b0f4a0dd1fcb951b8cd953596", null ],
    [ "INy_pin", "classmotorDriver_1_1DRV8847__channel.html#a552448e677684bc6ece89515e31b18fb", null ],
    [ "tch1", "classmotorDriver_1_1DRV8847__channel.html#a0cc74cb794bbc5fbb88bf773ec5e1a4c", null ],
    [ "tch2", "classmotorDriver_1_1DRV8847__channel.html#a3dbee2e8a24ea80b7c6f761dd03d9dbd", null ]
];