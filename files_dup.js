var files_dup =
[
    [ "405_hw2.py", "405__hw2_8py.html", null ],
    [ "405_hw4.py", "405__hw4_8py.html", null ],
    [ "405_lab1.py", "405__lab1_8py.html", "405__lab1_8py" ],
    [ "405_lab3.py", "405__lab3_8py.html", "405__lab3_8py" ],
    [ "405_lab4.py", "405__lab4_8py.html", "405__lab4_8py" ],
    [ "405_labFF.py", "405__labFF_8py.html", null ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "collectTask.py", "collectTask_8py.html", "collectTask_8py" ],
    [ "control_task.py", "control__task_8py.html", "control__task_8py" ],
    [ "controlTask.py", "controlTask_8py.html", [
      [ "controlTask", "classcontrolTask_1_1controlTask.html", "classcontrolTask_1_1controlTask" ]
    ] ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "data_task.py", "data__task_8py.html", "data__task_8py" ],
    [ "dataTask.py", "dataTask_8py.html", [
      [ "dataTask", "classdataTask_1_1dataTask.html", "classdataTask_1_1dataTask" ]
    ] ],
    [ "encoder_task.py", "encoder__task_8py.html", "encoder__task_8py" ],
    [ "encoderDriver.py", "encoderDriver_8py.html", "encoderDriver_8py" ],
    [ "encoderTask.py", "encoderTask_8py.html", [
      [ "encoderTask", "classencoderTask_1_1encoderTask.html", "classencoderTask_1_1encoderTask" ]
    ] ],
    [ "getChange.py", "getChange_8py.html", "getChange_8py" ],
    [ "getTotal.py", "getTotal_8py.html", "getTotal_8py" ],
    [ "hw2_elevator_fsm.py", "hw2__elevator__fsm_8py.html", "hw2__elevator__fsm_8py" ],
    [ "lab0xFF.py", "lab0xFF_8py.html", "lab0xFF_8py" ],
    [ "lab0xFF_nucleo_main.py", "lab0xFF__nucleo__main_8py.html", "lab0xFF__nucleo__main_8py" ],
    [ "lab1.py", "lab1_8py.html", "lab1_8py" ],
    [ "lab2.py", "lab2_8py.html", "lab2_8py" ],
    [ "lab3.py", "lab3_8py.html", "lab3_8py" ],
    [ "labFF_main.py", "labFF__main_8py.html", null ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "motor_task.py", "motor__task_8py.html", "motor__task_8py" ],
    [ "motorDriver.py", "motorDriver_8py.html", "motorDriver_8py" ],
    [ "motorTask.py", "motorTask_8py.html", [
      [ "motorTask", "classmotorTask_1_1motorTask.html", "classmotorTask_1_1motorTask" ]
    ] ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "simonTask.py", "simonTask_8py.html", [
      [ "simonTask", "classsimonTask_1_1simonTask.html", "classsimonTask_1_1simonTask" ]
    ] ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touch_task.py", "touch__task_8py.html", "touch__task_8py" ],
    [ "touchDriver.py", "touchDriver_8py.html", "touchDriver_8py" ],
    [ "ui_task.py", "ui__task_8py.html", "ui__task_8py" ]
];